//@(#) eeprom.h

#ifndef EEPROM_H
#define EEPROM_H

/*Other EEPROM values:
    Index           Value
    0               set clock bit
*/

const uint8_t start_index = 30;

void updateEEPROM() { //update EEPROM if necessary
  uint16_t index = start_index;
  EEPROMr.put(index, state);
  //EEPROM.put(index += sizeof(st_state), netconf);

  EEPROMr.commit();
}

void loadEEPROM() { //get settings from EEPROM
  uint16_t index = start_index;
  EEPROMr.get(index, state);
  //EEPROM.get(index += sizeof(st_state), netconf);
}


#endif
