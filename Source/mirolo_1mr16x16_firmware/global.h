//@(#) global.h

#ifndef GLOBAL_H
#define GLOBAL_H

//Device specific settings. Set the MAC address, RGB color calibration in % and a unique serial number.
#if SERIALNR == 7
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x1A, 0x14, 0x34}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
#define serial_nr "2018.11.0007"
#endif
#if SERIALNR == 8
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x1D, 0xCD, 0xE1}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
#define serial_nr "2019.05.0008"
#endif
#if SERIALNR == 9
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x21, 0x87, 0x8D}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
#define serial_nr "2019.05.0009"
#endif
#if SERIALNR == 10
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x60, 0x09, 0x7F}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
#define serial_nr "2022.07.0010"
#endif
#if SERIALNR == 11
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x41, 0x9B, 0x61}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
#define serial_nr "2022.07.0011"
#endif
#if SERIALNR == 12
const byte mac[] PROGMEM = {0x20, 0xDC, 0x93, 0x7D, 0x36, 0x2B}; //Prefix 20-DC-93 Cheetah Hi-Tech, Inc. + HEX(flip_horiz(SNR))
const uint8_t calibration_R = 100;
const uint8_t calibration_G = 50;
const uint8_t calibration_B = 63;
#define serial_nr "2022.07.0012"
#endif

enum direction : uint8_t {up=0, right, down, left};

//global settings

const char* ssid = "SSID"; //wifi network
const char* password = "PW";

const uint8_t serial_stringsize = 100;
const unsigned long serial_baud = 115200;

const uint8_t i2cAddress = 30;
const uint8_t i2cDelay = 2;

IPAddress ntpIP(192, 168, 178, 1); //NTP Server
//IPAddress ntpIP(192, 168, 130, 1); //NTP Server
const int timeZone = 2; //Greenwich + ?

struct text_preset {
  const char t0[80];
  const char t1[80];
  const char t2[80];
  const char t3[80];
  const char t4[80];
  const char t5[80];
  const char t6[80];
  const char t7[80];
  const char t8[80];
};

text_preset text_gc = {
  "Next Please",
  "Info      Lost+Found     Tickets",
  "GalaCon 2022 /)(\\",
  "Standard Tickets",
  "Bizaam + Plus Tickets",
  "GalaRun Results Check In",
  "Volunteer + Vendor Check In",
  "",
  ""
};

text_preset text_ef = {
  "Next Please",
  "EF27 <> Black Magic",
  "Art Show",
  "Art Show - Artist Check In",
  "Art Show - Art Pickup today until 17:00",
  "Art Show - Art Pickup will resume after the PawPet Show",
  "Registration Forms ++ Rules ++ Helper Sheets",
  "C",
  "Art Pickup Stations A + B + C"
};

text_preset * text = &text_gc;


struct st_state {
  uint8_t menu = 0;
  uint8_t sub_menu = 10;
  uint8_t last_menu = 0;
  uint8_t brightness = 50;
  direction rotation = up;
  direction orientation = up;
  uint8_t text = 0;
};





//global variables
EEPROM_Rotate EEPROMr;
boolean settingsChanged = false;

bool REST_api_enabled = true;
bool REST_html_enabled = true;

WiFiServer server(80);
WiFiClient client;

char ser_inputString[serial_stringsize] = {'\0'};

uint8_t method = 0; //http Method (UNDEFINED/GET/POST/....)
const uint8_t url_size = 50;
const uint8_t frame_size = 25;
char url[url_size] = {'\0'}; //http receive buffer
const uint16_t recv_content_size = 500;
char recv_content[recv_content_size] = {'\0'}; //http receive buffer
char session[17] = {"0"}; //http session cookie 128bit/16byte
boolean authOK = false; //http authentification
boolean sessionOK = false; //http session

char pin[5] = {HTTPPASSWD}; //access password
char username[21] = {"\0"}; //access username

Adafruit_NeoMatrix * matrix;
    
/*
//the display parameters. changes to these may not be applied immediatly
struct miroloDISPLAY {
  char    message0[max_bufsize];   //line0
  char    prefix0[4];
  uint8_t scrolldelay0;
  char    message1[max_bufsize];   //line1
  char    prefix1[4];
  uint8_t scrolldelay1;

  uint8_t brightness;
  uint32_t color;
  char animation[20];
  boolean pwr;
  uint8_t apply_changes;
};
*/

//the display hardware parameters. changes to these are applied immediatly
struct miroloHW {
  boolean htmlON;
  boolean restON;
  char Name[25];
  const char HW[20];
  const char CPU[20];
  const char SNR[13];
  char FW[25];
  char MAC[18];
  char IP[16];
};


miroloHW mirolo = {true, true, "Waysign 1", "mirolo 1MR16X16", "esp8266@80MHz", serial_nr, "", "", ""};


st_state state;
#endif
