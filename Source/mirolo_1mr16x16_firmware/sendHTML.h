//@(#) sendHTML.h

#ifndef SENDHTML_H
#define SENDHTML_H

enum snr {num1, seq_nr_count};


//global variables
//DynamicJsonBuffer jsonBuffer(3000);
StaticJsonBuffer<900> jsonBuffer;
const uint16_t msg_size = 900;
char msg[msg_size] = {'\0'};

unsigned long seq_numbers[seq_nr_count] = {0};


//prints 32-bit data in hex in html notation
char * printHEX32(const uint32_t &data) {
  static char tmp[16];
  sprintf(tmp, "#%.6x", data);
  return tmp;
}


//split string "/api/1234/temp" into parts "api" "1234" "temp", returns char ** to 2d matrix containing strings
const char ** splitString(const char * url, const uint16_t &url_size, const char * delim) {
  const uint8_t split_max = 20;
  static char ** matrix = NULL;

  //clear matrix
  if (matrix != NULL) {
    uint8_t counter = 0;
    while (counter < split_max && matrix[counter] != NULL) {
      delete [] matrix[counter];
      counter++;
    }

    delete [] matrix;
  }

  //create new matrix and init with NULL
  matrix = new char *[split_max];
  for (uint8_t i = 0; i < split_max; ++i) {
    matrix[i] = NULL;
  }

  //copy url
  char * url_cpy = new char[url_size];
  strcpy(url_cpy, url);

  //split url into fragments
  uint8_t counter = 0;
  char * token = strtok(url_cpy, delim);
  while (counter < (split_max - 1) && token != 0) { //last pointer always remains NULL
    matrix[counter] = new char[strlen(token) + 1];
    strcpy(matrix[counter], token);
    token = strtok(NULL, delim);
    ++counter;
  }

  //clean up
  delete [] url_cpy;

  return (const char **)matrix;
}




//send c-string in flash to client or calculate characters to send
unsigned int Send_PROGMEM(WiFiClient &client, const boolean &transmit, const __FlashStringHelper* p_text) { //send PROGMEM String to client, returns length of String

  if (transmit) {
    client.print(p_text);
  }
  return strlen_P((const char*)p_text);
}



//send c-string to client or calculate characters to send
unsigned int Send_String(WiFiClient &client, const boolean &transmit, const char* p_text) { //send String to client, returns length of String

  if (transmit) {
    client.print(p_text);
  }
  return strlen(p_text);
}


//encode html special characters to prevent cross-site-scripting
uint16_t countHTMLspecialchars(const char * data) {
  uint16_t count = 0;
  for (uint16_t pos = 0; pos <= strlen(data); ++pos) {
    switch (data[pos]) {
      case '&':  count += 5;  break;
      case '\"': count += 6;  break;
      case '\'': count += 6;  break;
      case '<':  count += 4;  break;
      case '>':  count += 4;  break;
      default:   count++;   break;
    }
  }
  return count;
}

char * encodeHTMLspecialchars(const char * data) {
  static char * encoded = NULL;

  if (encoded) {
    delete [] encoded;
    encoded = NULL;
  }
  encoded = new char[countHTMLspecialchars(data)];
  encoded[0] = '\0';

  for (uint16_t pos = 0; pos <= strlen(data); ++pos) {
    switch (data[pos]) {
      case '&':  strcat_P(encoded, PSTR("&amp;"));          break;
      case '\"': strcat_P(encoded, PSTR("&quot;"));         break;
      case '\'': strcat_P(encoded, PSTR("&apos;"));         break;
      case '<':  strcat_P(encoded, PSTR("&lt;"));           break;
      case '>':  strcat_P(encoded, PSTR("&gt;"));           break;
      default:   strncat(encoded, &data[pos], 1);   break;
    }
  }
  return encoded;
}





//generate and send the HTML index page or calculate its character size before sending
unsigned int generateHTMLindex(WiFiClient &client, const boolean &transmit, char * contentType, uint16_t * statusCode) {
  const uint16_t count_init = __COUNTER__;
  static uint16_t line = count_init;
  static bool isclient = false;
  unsigned int content_length = 0;
  bool ressourceFound = false;
  static char ** url_matrix = NULL;

  //get url parts
  if (!transmit && line == count_init) {
    url_matrix = (char **)splitString(url, url_size, "/\n");
    isclient = false;
    if (url_matrix[0] != NULL) {
      char * response_data = serialRequest("isclient", url_matrix[0], NULL);
      if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL && atoi(response_data) == 1) isclient = true;
    }
  }

  line++;

  //favicon
  if (url_matrix[0] != NULL && strcmp_P(url_matrix[0], PSTR("favicon.ico")) == 0) {
    line = count_init;

    strcpy_P(contentType, PSTR("text/plain"));
    *statusCode = 404;
  }
  //html
  else {
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<!DOCTYPE html>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<html>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<head>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<meta charset=\"utf-8\">\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" >\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<title id=\"title\">Centra Heating Control</title>\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<style>\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("body{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #202935;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    font-family: Cantarell, Arial;}\n\n"));

    /*top navigation bar*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    position: relative;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    width: 100%;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #2a303d;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    overflow: hidden;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    font-size: 30pt;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    white-space: nowrap;}\n\n"));


    /* Style the links/text inside the navigation bar */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a, .topnav span {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    float: left;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    color: #f2f2f2;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    text-align: center;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding: 0.3em 0.8em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    text-decoration: none;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    cursor:default;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    cursor:pointer;}\n\n"));

    /* Add a color to the home link */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".topnav a.home, .topnav span.home{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #5e0b24;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding-right: 0.3em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    color: white;}\n\n"));

    /* Flex horizontal*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".row-flex {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  margin-top: 3em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  margin-bottom: 3em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  display: flex;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  flex-direction: row;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  align-items: center;}\n\n"));

    /** Flex vertical **/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".vert-flex {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  display: flex;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  flex-direction: column;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  align-items: center;}\n\n"));







    /*toggle switch*/
    /* The switch - the box around the slider */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".switch {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  position: relative;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  display: inline-block;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  width: 1.7em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  height: 1em;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".switchfloatright {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  position: relative;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  float: right;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  margin-top: 0.5em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  margin-right: 1em;}\n\n"));

    /* Hide default HTML checkbox */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".switch input {display:none;}\n\n"));

    /* The slider */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".toggleslider {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  position: absolute;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  cursor: pointer;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  top: 0;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  left: 0;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  right: 0;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  bottom: 0;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  background-color: #7e7e7e;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  border-radius: 1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  -webkit-transition: .2s;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  transition: .2s;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".toggleslider:before {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  position: absolute;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  content: \"\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  height: 0.8em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  width: 0.8em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  left: 0.1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  bottom: 0.1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  background-color: white;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  border-radius: 50%;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  -webkit-transition: .2s;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  transition: .2s;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("input:checked + .toggleslider {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  background-color: #5e0b24;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("input:checked + .toggleslider:before {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  -webkit-transform: translateX(0.7em);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  -ms-transform: translateX(0.7em);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("  transform: translateX(0.7em);}\n\n"));




    /*textbox*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".textbox {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #2a303d;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    color: white;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding: 1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    font-size: 16pt;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    margin-bottom: 1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border: none;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    cursor:default;}\n\n"));


    /*standard div box*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".contentbox {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    width: 98%;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    min-height: 12em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #44546a;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding: 0.5em 0.5em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    margin: 1em 0.5em 1em;}\n\n"));


    /* Temperature info */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".temp{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    margin-left: 0.2em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    font-size: 100pt;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    color: white;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".w2{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    min-width: 2em;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".w4{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    min-width: 4em;}\n\n"));






    /*info notification*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".error {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #ffdddd;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border-left: 0.3em solid #f44336;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".success {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #ddffdd;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border-left: 0.3em solid #4CAF50;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".info {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #e7f3fe;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border-left: 0.3em solid #2196F3;}\n\n"));


    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".warning {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #ffffcc;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border-left: 0.3em solid #ffeb3b;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".system {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #ddccff;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border-left: 0.3em solid #9b3bff;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".messagediv{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    margin: 1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding: 0.1em 0.3em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    font-size: 18pt;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    cursor:default;}\n\n"));




    /* Confirm Button */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".confirmbtn {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    color: white;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding: 1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    min-width: 3em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    font-size: 20pt;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    margin: 0.3em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border: none;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    cursor: pointer;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    float: left;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".apply .confirmbtn{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #620123;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".cancel .confirmbtn{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #3c2a2c;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".disable .confirmbtn{\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #747474;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".apply:hover .confirmbtn {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #70142e;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".cancel:hover .confirmbtn {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #322123;}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".disable:hover .confirmbtn {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #747474;}\n\n"));

    /* Dropdown Button */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropbtn {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #620123;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    color: white;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    min-width: 7em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding: 1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    font-size: 20pt;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    margin: 0.3em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    border: none;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    cursor: pointer;}\n\n"));


    /* The container <div> - needed to position the dropdown content */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    position: relative;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    display: inline-block;}\n\n"));

    /* Dropdown Content (Hidden by Default) */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown-content {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    display: none;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    position: absolute;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #f9f9f9;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    left: -1em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    top: -10em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    min-width: 8em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    box-shadow: 0 0.2em 0.3em 0 rgba(0,0,0,0.2);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    z-index: 4;}\n\n"));

    /* Links inside the dropdown */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown-content a {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    color: black;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    padding: 0.4em 0.8em;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    text-decoration: none;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    display: block;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    cursor: pointer;}\n\n"));

    /* Change color of dropdown links on hover */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown-content a:hover {background-color: #e1e1e1}\n\n"));

    /* Show the dropdown menu on hover */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown:hover .dropdown-content {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    display: block;}\n\n"));

    /* Change the background color of the dropdown button when the dropdown content is shown */
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(".dropdown:hover .dropbtn {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    background-color: #70142e;}\n\n"));




    /*color picker*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("input[type=\"color\"] {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("    display: none;}\n\n"));




    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\n}\n</style>\n</head>\n<body>\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"topnav\" id=\"siteTopnav\">\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"home\" id=\"time\">"));
    if (!transmit || line == __COUNTER__) {
      char tmp[10] = {'\0'};
      itoa(hour(), tmp, 10);
      content_length += Send_String(client, transmit, tmp);
      content_length += Send_PROGMEM(client, transmit, F(":"));
      if (minute() < 10) content_length += Send_PROGMEM(client, transmit, F("0"));
      itoa(minute(), tmp, 10);
      content_length += Send_String(client, transmit, tmp);
    }
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"home\"> | </span>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a class=\"home\" onclick=\"location.href='/';\">Home</a>\n"));
    if (!transmit || line == __COUNTER__) {
      uint8_t counter = 0;
      char url_build[url_size] = {'\0'};
      while (url_matrix[counter] != NULL) {
        strcat(url_build, "/");
        strcat(url_build, url_matrix[counter]);

        content_length += Send_PROGMEM(client, transmit, F("<span class=\"home\"> > </span>\n"));
        content_length += Send_PROGMEM(client, transmit, F("<a class=\"home\" onclick=\"location.href='"));
        content_length += Send_String(client, transmit, url_build);
        content_length += Send_PROGMEM(client, transmit, F("';\">"));
        if (isclient && counter == 0) {
          char * response_data = serialRequest(url_matrix[0], F("description"), NULL);
          if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) content_length += Send_String(client, transmit, response_data);
          else content_length += Send_String(client, transmit, url_matrix[counter]);
        }
        else content_length += Send_String(client, transmit, url_matrix[counter]);
        content_length += Send_PROGMEM(client, transmit, F("</a>\n\n"));
        ++counter;
      }
    }


    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<!-- Rounded switch -->\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"switchfloatright\">\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<label class=\"switch\">\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input type=\"checkbox\" checked id=\"pwr\" onchange=\"patchAttribute('/system/power', 'power', (this.checked ? 1 : 0), this, 500)\">\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"toggleslider round\"></span>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</label>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n\n"));

    //index
    if (strcmp(url, "/") == 0) {
      ressourceFound = true;
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"contentbox\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox\">Rooms</div>\n"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest("system", F("clients"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          char clients[serial_stringsize] = {'\0'};
          strcpy(clients, response_data);
          char * token = strtok(clients, ",\n");
          while (token != 0) {
            char * response_data = serialRequest(token, F("description"), NULL);
            content_length += Send_PROGMEM(client, transmit, F("<div class=\"apply\">\n"));
            content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" onclick=\"location.href='/"));
            content_length += Send_String(client, transmit, token);
            content_length += Send_PROGMEM(client, transmit, F("';\">"));
            if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) content_length += Send_String(client, transmit, response_data);
            else content_length += Send_String(client, transmit, token);
            content_length += Send_PROGMEM(client, transmit, F("</button>\n"));
            content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
            token = strtok(NULL, ",\n");
          }
        }
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n\n"));
    }

    //client
    else if (strstr(url, "/") != NULL && url_matrix[0] != NULL && isclient) {
      ressourceFound = true;
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"contentbox\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox\">Info</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"row-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"temp\" id=\"celsius\">"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("celsius"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) content_length += Send_String(client, transmit, response_data);
        else content_length += Send_PROGMEM(client, transmit, F("--"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span><span class=\"temp w2\" id=\"valve\">°</span>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"temp\" id=\"humid\">"));
      static uint8_t humidity = 0;
      humidity = 0;
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("humidity"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          content_length += Send_String(client, transmit, response_data);
          humidity = atol(response_data);
        }
        else content_length += Send_PROGMEM(client, transmit, F("--"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span><span class=\"temp w2\">%</span>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"row-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"temp w2\" id=\"humhigh\" style=\"color:#4e4efe;"));
      static uint8_t humiditywarn = 0;
      humiditywarn = 0;
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("humiditywarn"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) humiditywarn = atol(response_data);
        if (humidity < humiditywarn) content_length += Send_PROGMEM(client, transmit, F("display:none;"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" title=\"Humidity High!\">♨</span>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"temp w2\" id=\"winopen\" style=\"color:#8e0b05;"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("windowopen"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL && atol(response_data) == 0) content_length += Send_PROGMEM(client, transmit, F("display:none;"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" title=\"Window Open!\">◫</span>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"apply\">\n"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("forceheatnight"), NULL);
        content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"h_up\" "));
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL && atol(response_data) == 2) content_length += Send_PROGMEM(client, transmit, F("style=\"display:none\" "));
        content_length += Send_PROGMEM(client, transmit, F("onclick=\"patchAttribute('/' + client_id + '/forceheatnight', 'forceheatnight', this.textContent == 'Heat up now' ? 1 : 0, this, 10)\">"));
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL && atol(response_data) == 0) content_length += Send_PROGMEM(client, transmit, F("Heat up now"));
        else content_length += Send_PROGMEM(client, transmit, F("Stop heating"));
        content_length += Send_PROGMEM(client, transmit, F("</button>\n"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n\n"));

      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"contentbox\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox\">Settings</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"row-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"temp w4\"><span class=\"temp\">☼ </span><span class=\"temp\" id=\"cday\">"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("celsiusday"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) content_length += Send_String(client, transmit, response_data);
        else content_length += Send_PROGMEM(client, transmit, F("--"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span><span class=\"temp\">°</span></span>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"vert-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"apply\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"td+\" onclick=\"patchAttribute('/' + client_id + '/celsiusday', 'celsiusday', getInt('cday') + 1, this, 10)\">▲</button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"cancel\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"td-\" onclick=\"patchAttribute('/' + client_id + '/celsiusday', 'celsiusday', getInt('cday') - 1, this, 10)\">▼</button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n\n"));

      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"row-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"temp w4\"><span class=\"temp\">☾</span><span class=\"temp\" id=\"cnight\">"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("celsiusnight"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) content_length += Send_String(client, transmit, response_data);
        else content_length += Send_PROGMEM(client, transmit, F("--"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span><span class=\"temp\">°</span></div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"vert-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"apply\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"tn+\" onclick=\"patchAttribute('/' + client_id + '/celsiusnight', 'celsiusnight', getInt('cnight') + 1, this, 10)\">▲</button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"cancel\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"tn-\" onclick=\"patchAttribute('/' + client_id + '/celsiusnight', 'celsiusnight', getInt('cnight') - 1, this, 10)\">▼</button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"w4\"></span>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"vert-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"dropbtn\">Start <strong><span id=\"nstart\">"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("nightmodestart"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          char tmp[10] = {'\0'};
          uint16_t t = atol(response_data);
          ltoa(t / 100, tmp, 10);
          content_length += Send_String(client, transmit, tmp);
        }
        else content_length += Send_PROGMEM(client, transmit, F("--"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(":00</span></strong></button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodestart', 'nightmodestart', parseInt(this.textContent)*100, this, 'nstart')\">20:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodestart', 'nightmodestart', parseInt(this.textContent)*100, this, 'nstart')\">21:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodestart', 'nightmodestart', parseInt(this.textContent)*100, this, 'nstart')\">22:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodestart', 'nightmodestart', parseInt(this.textContent)*100, this, 'nstart')\">23:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodestart', 'nightmodestart', parseInt(this.textContent)*100, this, 'nstart')\">0:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodestart', 'nightmodestart', parseInt(this.textContent)*100, this, 'nstart')\">1:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodestart', 'nightmodestart', parseInt(this.textContent)*100, this, 'nstart')\">2:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"dropbtn\">End <strong><span id=\"nend\">"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("nightmodeend"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          char tmp[10] = {'\0'};
          uint16_t t = atol(response_data);
          ltoa(t / 100, tmp, 10);
          content_length += Send_String(client, transmit, tmp);
        }
        else content_length += Send_PROGMEM(client, transmit, F("--"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(":00</span></strong></button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodeend', 'nightmodeend', parseInt(this.textContent)*100, this, 'nend')\">4:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodeend', 'nightmodeend', parseInt(this.textContent)*100, this, 'nend')\">5:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodeend', 'nightmodeend', parseInt(this.textContent)*100, this, 'nend')\">6:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodeend', 'nightmodeend', parseInt(this.textContent)*100, this, 'nend')\">7:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodeend', 'nightmodeend', parseInt(this.textContent)*100, this, 'nend')\">8:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodeend', 'nightmodeend', parseInt(this.textContent)*100, this, 'nend')\">9:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/nightmodeend', 'nightmodeend', parseInt(this.textContent)*100, this, 'nend')\">10:00</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"row-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"dropbtn\">Humidity Warning at <strong><span id=\"hthresh\">"));
      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("humiditywarn"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          content_length += Send_String(client, transmit, response_data);
        }
        else content_length += Send_PROGMEM(client, transmit, F("--"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("%</span></strong></button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">30%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">35%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">40%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">45%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">50%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">55%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">60%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">65%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">70%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">75%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/humiditywarn', 'humiditywarn', parseInt(this.textContent), this, 'hthresh')\">80%</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"dropbtn\">Warning Timeout: <strong><span id=\"wtimeout\">"));

      if (!transmit || line == __COUNTER__) {
        char * response_data = serialRequest(url_matrix[0], F("warntimeout"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          char tmp[10] = {'\0'};
          int16_t t = atol(response_data);
          if (t == 0) {
            strcat_P(tmp, PSTR("never"));
          }
          else if (t > 0) {
            strcat_P(tmp, PSTR("+"));
            strcat(tmp, response_data);
            strcat_P(tmp, PSTR("min"));
          }
          else if (t < 0) {
            strcat(tmp, response_data);
            strcat_P(tmp, PSTR("min"));
          }
          content_length += Send_String(client, transmit, tmp);
        }
        else content_length += Send_PROGMEM(client, transmit, F("never"));
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</span></strong></button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"dropdown-content\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">-30min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">-20min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">-10min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">-5min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">-1min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">never</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">+1min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">+5min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">+10min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">+20min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<a onclick=\"dropdownChange('/' + client_id + '/warntimeout', 'warntimeout', parseInt(this.textContent), this, 'wtimeout')\">+30min</a>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"row-flex\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"apply\">\n"));
      static char color[10] = {'\0'};
      if (!transmit || line == __COUNTER__) {
        color[0] = '\0';
        char * response_data = serialRequest(url_matrix[0], F("colorbacklight"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL && strlen(response_data) < 10) {
          uint32_t col = strtol(response_data, '\0', 10);
          strcpy(color, printHEX32(col));
        }
      }
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"c_bl\" style=\"border-style:solid;border-color:"));
      if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, color);
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(";\" onclick=\"colorpickActivate('pick_bl')\">Backlight</button>\n"));
      if (!transmit || line == __COUNTER__) {
        color[0] = '\0';
        char * response_data = serialRequest(url_matrix[0], F("colorwindow"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL && strlen(response_data) < 10) {
          uint32_t col = strtol(response_data, '\0', 10);
          strcpy(color, printHEX32(col));
        }
      }

      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input type=\"color\" value=\""));
      if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, color);
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" id=\"pick_bl\" oninput=\"colorpickInput('pick_bl', 'c_bl')\" onchange=\"patchAttribute('/' + client_id + '/colorbacklight', 'colorbacklight', this.value, this, 10)\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class = \"apply\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id =\"c_win\" style =\"border-style:solid;border-color:"));
      if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, color);
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(";\" onclick=\"colorpickActivate('pick_win')\">Window Warning</button>\n"));
      if (!transmit || line == __COUNTER__) {
        color[0] = '\0';
        char * response_data = serialRequest(url_matrix[0], F("colorhumidity"), NULL);
        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL && strlen(response_data) < 10) {
          uint32_t col = strtol(response_data, '\0', 10);
          strcpy(color, printHEX32(col));
        }
      }

      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input type=\"color\" value=\""));
      if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, color);
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" id=\"pick_win\" oninput=\"colorpickInput('pick_win', 'c_win')\" onchange=\"patchAttribute('/' + client_id + '/colorwindow', 'colorwindow', this.value, this, 10)\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(" </div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class = \"apply\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<button class=\"confirmbtn\" id=\"c_hum\" style=\"border-style:solid;border-color:"));
      if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, color);
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F(";\" onclick=\"colorpickActivate('pick_hum')\">Humidity Warning</button>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<input type=\"color\" value=\""));
      if (!transmit || line == __COUNTER__)content_length += Send_String(client, transmit, color);
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\" id=\"pick_hum\" oninput=\"colorpickInput('pick_hum', 'c_hum')\" onchange=\"patchAttribute('/' + client_id + '/colorhumidity', 'colorhumidity', this.value, this, 10)\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));

      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</div>\n\n"));
    }


    //404
    if (!ressourceFound) {
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"contentbox\">\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<div class=\"textbox\">404 Not Found</div>\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<span class=\"temp\"><strong style=\"color: #ac393e\">&#9888;</strong> 404</span></div>\n\n"));
    }




    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("<script>\n\n"));

    /* client identifier*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("const client_id = \""));
    if (!transmit || line == __COUNTER__) {
      if (url_matrix[0] != NULL && isclient) content_length += Send_String(client, transmit, url_matrix[0]);
      else content_length += Send_PROGMEM(client, transmit, F("index"));
    }
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("\";\n\n"));
    /* global sequence number*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("let seq_number = 0;\n\n"));
    /*AJAX request timers*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("let timers = [];\n\n"));



    /*functions*/

    //color picker
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function colorpickInput(id, button) {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(button).style.borderColor = document.getElementById(id).value;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function colorpickActivate(id){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(id).click();}\n\n"));

    //update dropdown
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function dropdownChange(url, attribute, value, element, span) {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(span).textContent = element.textContent;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("patchAttribute(url, attribute, value, element, 10);}\n\n"));

    //get int contained in element
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function getInt(id){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("return parseInt(document.getElementById(id).textContent);}\n\n"));

    //patch
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function patchAttribute(url, attribute, value, element, delay){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("contentObj = {};\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("contentObj[attribute] = value;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(url, contentObj, \"PATCH\", function() {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateSite(JSON.parse(this.responseText));\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, element.id, delay);}\n\n"));

    /*AJAX request*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function sendAJAXrequest(url, content, method, callback, id, delay){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("callback = callback || null;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(timers[id])clearTimeout(timers[id]);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("timers[id] = setTimeout(function() {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("seq_number++;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("var xhttp = new XMLHttpRequest();\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(callback)xhttp.onreadystatechange = callback;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("xhttp.open(method, \"/api\" + url, true);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(content)content[\"sequencenumber\"] = seq_number;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("xhttp.send(JSON.stringify(content));\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, delay);}\n\n"));


    /*AJAX site update | heartbeat*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function enableAJAXupdate(url, id, delay){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(timers[id])clear.Interval(timers[id]);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("timers[id] = setInterval(function() {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("sendAJAXrequest(url, null, \"GET\", function() {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if (this.readyState == 4 && this.status == 200) {\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("updateSite(JSON.parse(this.responseText));\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, id + \"_exec\", 5);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("}, delay);}\n\n"));

    if (strstr(url, "/") != NULL && url_matrix[0] != NULL && isclient) {
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/celsius\", \"hb_celsius\", 5000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/celsiusday\", \"hb_celsiusd\", 20000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/celsiusnight\", \"hb_celsiusn\", 20000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/humidity\", \"hb_humid\", 10000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/forceheatnight\", \"hb_forcen\", 10000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/humiditywarn\", \"hb_humwarn\", 20000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/windowopen\", \"hb_winopen\", 3000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/colorbacklight\", \"hb_cbackl\", 30000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/colorwindow\", \"hb_cwind\", 30000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/colorhumidity\", \"hb_chumid\", 30000);\n"));
      if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/\" + client_id + \"/warntimeout\", \"hb_wtout\", 15000);\n"));
    }
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/system/time\", \"hb_time\", 10000);\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("enableAJAXupdate(\"/system/power\", \"hb_power\", 4000);\n\n"));




    /*site update*/
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("function updateSite(json){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.hasOwnProperty(\"data\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("let element = null;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"hour\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"time\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element.textContent = json.data.hour + \":\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.minute < 10) element.textContent += \"0\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element.textContent += json.data.minute;   }\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"celsiusday\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"cday\").textContent = json.data.celsiusday;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"celsiusnight\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"cnight\").textContent = json.data.celsiusnight;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"celsius\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"celsius\").textContent = json.data.celsius;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"humidity\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"humid\").textContent = json.data.humidity;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"valve\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"valve\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.valve === 0) element.textContent = \"°↓\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(json.data.valve <= 30) element.textContent = \"°\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else element.textContent = \"°↑\";}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"forceheatnight\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"h_up\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.forceheatnight === 2) element.style.display = \"none\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else element.style.display = \"inline\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.forceheatnight === 1) element.textContent = \"Stop heating\";  \n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if(json.data.forceheatnight === 0) element.textContent = \"Heat up now\";}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"windowopen\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"winopen\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.windowopen === 0) element.style.display = \"none\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else element.style.display = \"inline\";}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"humiditywarn\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"humhigh\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.humiditywarn >= getInt(\"humid\")) element.style.display = \"none\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else element.style.display = \"inline\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"hthresh\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element.textContent = json.data.humiditywarn + \"%\";}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"warntimeout\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"wtimeout\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.warntimeout === 0) element.textContent = \"never\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else if (json.data.warntimeout > 0) element.textContent = \"+\" + json.data.warntimeout + \"min\";\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else element.textContent = json.data.warntimeout + \"min\";}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"nightmodestart\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"nstart\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element.textContent = (json.data.nightmodestart/100) + \":00\";}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"nightmodeend\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"nend\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element.textContent = (json.data.nightmodeend/100) + \":00\";}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"colorbacklight\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"c_bl\").style.borderColor = json.data.colorbacklight;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"pick_bl\").value = json.data.colorbacklight;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"colorwindow\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"c_win\").style.borderColor = json.data.colorwindow;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"pick_win\").value = json.data.colorwindow;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"colorhumidity\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"c_hum\").style.borderColor = json.data.colorhumidity;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("document.getElementById(\"pick_hum\").value = json.data.colorhumidity;}\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.hasOwnProperty(\"power\")){\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("element = document.getElementById(\"pwr\");\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("if(json.data.power === 0) element.checked = false;\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("else element.checked = true;}}}}\n\n"));

    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</script>\n"));
    if (!transmit || line == __COUNTER__)content_length += Send_PROGMEM(client, transmit, F("</body>\n"));
    if (!transmit || line == __COUNTER__) {
      content_length += Send_PROGMEM(client, transmit, F("</html>\n"));
      line = count_init;
    }


    strcpy_P(contentType, PSTR("text/html"));
    *statusCode = 200;
  }

  return content_length;
}



//generate and send API reply or calculate its character size before sending
unsigned int generateAPIreply(WiFiClient &client, const boolean &transmit, char * contentType, uint16_t * statusCode) {
  unsigned int content_length = 0;

  if (!transmit) {

    //reset
    msg[0] = '\0';

    const char ** url_array = splitString(url, url_size, "/\n");
    enum url_parts {SERVICE = 0, ID, ATTR};

    //create message
    if (method == GET || method == DELETE) {
      //no content. no parsing. no problem

      //routes
      if (strcmp_P(url_array[ATTR], PSTR("celsius")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("celsius"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("celsius")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("humidity")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("humidity"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("humidity")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/system/time")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        data[F("id")] = url_array[ID];
        data[F("hour")] = hour();
        data[F("minute")] = minute();
        data[F("second")] = second();

        *statusCode = 200;
        json[F("status")] = 200;
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/system/power")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("power"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("power")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url, PSTR("/api/system/clients")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        JsonArray& clients = data.createNestedArray(F("clients"));

        char * response_data = serialRequest(url_array[ID], F("clients"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];

          //copy
          char * response_cpy = new char[serial_stringsize];
          strcpy(response_cpy, response_data);

          //split into fragments
          uint8_t counter = 0;
          char * token = strtok(response_cpy, ",\n");
          while (token != 0) {
            clients.add(token);
            token = strtok(NULL, ",\n");
            ++counter;
          }

          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("celsiusday")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("celsiusday"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("celsiusday")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("celsiusnight")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("celsiusnight"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("celsiusnight")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("valve")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("valve"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("valve")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("forceheatnight")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("forceheatnight"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("forceheatnight")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("description")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("description"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("description")] = response_data;
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("request")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("request"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("request")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("humiditywarn")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("humiditywarn"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("humiditywarn")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("windowopen")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("windowopen"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("windowopen")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("dhtoffset")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("dhtoffset"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("dhtoffset")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("warntimeout")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("warntimeout"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("warntimeout")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("reboottimeout")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("reboottimeout"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("reboottimeout")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("nightmodestart")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("nightmodestart"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("nightmodestart")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("nightmodeend")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("nightmodeend"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("nightmodeend")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("colorbacklight")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("colorbacklight"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("colorbacklight")] = printHEX32(strtoul(response_data, '\0', 10));
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("colorhumidity")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("colorhumidity"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("colorhumidity")] = printHEX32(strtoul(response_data, '\0', 10));
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      else if (strcmp_P(url_array[ATTR], PSTR("colorwindow")) == 0 && method == GET) {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        char * response_data = serialRequest(url_array[ID], F("colorwindow"), NULL);

        if (response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("colorwindow")] = printHEX32(strtoul(response_data, '\0', 10));
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else {
          *statusCode = 424;
          json[F("status")] = 424;
        }

        //convert to string
        json.printTo(msg, msg_size);
      }
      //no matching route
      else {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));
        *statusCode = 404;
        json[F("status")] = 404;
        //convert to string
        json.printTo(msg, msg_size);
      }

    }
    else if (method >= POST) {
      //we expect JSON at these API routes


      JsonObject& json = jsonBuffer.parse(recv_content);
      //check for parsing failure
      boolean payloadOK = json.success();

      //routes
      if (payloadOK && strcmp_P(url_array[ATTR], PSTR("celsiusnight")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("celsiusnight")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("celsiusnight"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("celsiusnight")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("celsiusday")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("celsiusday")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("celsiusday"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("celsiusday")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("forceheatnight")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("forceheatnight")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("forceheatnight"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("forceheatnight")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("description")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("description")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("description"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("description")] = response_data;
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("humiditywarn")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("humiditywarn")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("humiditywarn"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("humiditywarn")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("dhtoffset")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("dhtoffset")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("dhtoffset"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("dhtoffset")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("warntimeout")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("warntimeout")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("warntimeout"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("warntimeout")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("reboottimeout")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("reboottimeout")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("reboottimeout"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("reboottimeout")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("nightmodestart")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("nightmodestart")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("nightmodestart"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("nightmodestart")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("nightmodeend")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("nightmodeend")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("nightmodeend"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("nightmodeend")] = atol(response_data);
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("colorbacklight")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("colorbacklight")];
        if (value) {
          char * colorString = new char[strlen(value) + 1];
          strcpy(colorString, value);
          colorString[0] = '0'; //html notation! override '#'
          uint32_t col = strtoul (colorString, '\0', 16);
          delete [] colorString;
          char tmp[15] = {'\0'};
          ltoa(col, tmp, 10);
          response_data = serialRequest(url_array[ID], F("colorbacklight"), tmp);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("colorbacklight")] = printHEX32(strtoul(response_data, '\0', 10));
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("colorhumidity")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("colorhumidity")];
        if (value) {
          char * colorString = new char[strlen(value) + 1];
          strcpy(colorString, value);
          colorString[0] = '0'; //html notation! override '#'
          uint32_t col = strtoul (colorString, '\0', 16);
          delete [] colorString;
          char tmp[15] = {'\0'};
          ltoa(col, tmp, 10);
          response_data = serialRequest(url_array[ID], F("colorhumidity"), tmp);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("colorhumidity")] = printHEX32(strtoul(response_data, '\0', 10));
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url_array[ATTR], PSTR("colorwindow")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("colorwindow")];
        if (value) {
          char * colorString = new char[strlen(value) + 1];
          strcpy(colorString, value);
          colorString[0] = '0'; //html notation! override '#'
          uint32_t col = strtoul (colorString, '\0', 16);
          delete [] colorString;
          char tmp[15] = {'\0'};
          ltoa(col, tmp, 10);
          response_data = serialRequest(url_array[ID], F("colorwindow"), tmp);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("colorwindow")] = printHEX32(strtoul(response_data, '\0', 10));
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      else if (payloadOK && strcmp_P(url, PSTR("/api/system/power")) == 0 && method == PATCH) {
        boolean parseOK = false; //at least one data field must match the request

        char * response_data = NULL;
        const char * value = json[F("power")];
        if (value) {
          response_data = serialRequest(url_array[ID], F("power"), value);
          parseOK = true;
        }

        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (parseOK && response_data != NULL && strstr_P(response_data, PSTR("error.")) == NULL) {
          data[F("id")] = url_array[ID];
          data[F("power")] = response_data;
          *statusCode = 200;
          json[F("status")] = 200;
        }
        else if (parseOK) {
          *statusCode = 424;
          json[F("status")] = 424;
        }
        else {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        //convert to string
        json.printTo(msg, msg_size);

      }
      //no matching route
      else {
        jsonBuffer.clear();
        JsonObject& json = jsonBuffer.createObject();
        JsonObject& data = json.createNestedObject(F("data"));

        if (payloadOK == false) {
          *statusCode = 400;
          json[F("status")] = 400;
        }
        else {
          *statusCode = 404;
          json[F("status")] = 404;
        }
        //convert to string
        json.printTo(msg, msg_size);
      }
    }
    else {
      //something went really wrong
      jsonBuffer.clear();
      JsonObject& json = jsonBuffer.createObject();
      JsonObject& data = json.createNestedObject(F("data"));

      *statusCode = 404;
      json[F("status")] = 404;
      //convert to string
      json.printTo(msg, msg_size);
    }

    //set content type
    strcpy_P(contentType, PSTR("application/json"));

  }


  content_length += Send_String(client, transmit, msg);
  return content_length;
}




#endif
