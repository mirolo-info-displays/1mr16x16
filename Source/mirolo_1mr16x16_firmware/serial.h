//@(#) serial.h

#ifndef SERIAL_H
#define SERIAL_H

//empty RX Buffer (until given character)
void clearRx(const char &delim = '\0') {
  uint32_t timer = millis();
  while (millis() - timer < 3) { //wait on Serial.available for 3ms
    if (Serial.available()) {
      char c = Serial.read();
      if (delim != '\0' && c == delim) break;
      timer = millis();
    }
  }
}

//serial request functions
bool readSerial(char * inputBuffer, uint16_t bufferSize) { //reads message from Serial input. Messages have to end with \n. returns true on NewMessageAvailable
  struct st_reader {
    uint16_t inBufferSize = 0;
    uint16_t bufferIndex = 0;
    char* inBuffer = NULL;
    bool readComplete = false;
    st_reader * next = NULL;
  };

  static st_reader * reader = NULL; //start of queue
  bool result = false;


  //find self
  st_reader * self_reader = reader;
  while (self_reader != NULL) {
    if (self_reader->inBuffer == inputBuffer) {
      break;
    }
    else self_reader = self_reader->next;
  }

  //register new
  if (self_reader == NULL) {

    if (reader == NULL) {
      reader = new st_reader;
      self_reader = reader;
    }
    else {
      st_reader * end_reader = reader;
      while (end_reader->next != NULL) {
        end_reader = end_reader->next;
      }
      end_reader->next = new st_reader;
      self_reader = end_reader->next;
    }

    self_reader->inBufferSize = bufferSize;
    self_reader->bufferIndex = 0;
    self_reader->inBuffer = inputBuffer;
    self_reader->readComplete = false;
    self_reader->inBuffer[0] = '\0'; //reset
  }

  //find next pending reader in queue
  st_reader * pending_reader = reader;
  while (pending_reader != NULL) {
    if (pending_reader->readComplete == false && pending_reader->bufferIndex > 0) {
      break;
    }
    else pending_reader = pending_reader->next;
  }
  if (pending_reader == NULL) pending_reader = self_reader; //take over if no other readers are processing messages


  //read serial
  if (pending_reader != NULL) {
    char inChar[2] = {'\0'};
    while (Serial.available() > 0 && pending_reader->readComplete == false && pending_reader->bufferIndex < pending_reader->inBufferSize - 1) {

      // read the bytes incoming from the client:
      inChar[0] = (char)Serial.read();
      ++pending_reader->bufferIndex;

      if (pending_reader->bufferIndex >= pending_reader->inBufferSize - 1) { //stop reading on buffer overflow
        clearRx('\n'); //clear Rx until start of next message
        inChar[0] = '\0';
        pending_reader->readComplete = true;
      }
      else if (inChar[0] == 10) { //stop reading if NewLine is sent (ASCII 10, '\n' or ctrl-J)
        inChar[0] = '\0';
        pending_reader->readComplete = true;
      }


      strcat(pending_reader->inBuffer, inChar); //add the next inChar to the string (concat)

    }
  }


  //deregister old
  if (self_reader->readComplete == true) {
    //find previos reader in list
    if (self_reader == reader) { //we are No 1!
      reader = self_reader->next;
      delete self_reader;
    }
    else { //We are not No 1...
      st_reader * prev_reader = reader;
      while (prev_reader->next != self_reader) {
        prev_reader = prev_reader->next;
      }
      prev_reader->next = self_reader->next; //jump self in list
      delete self_reader;
    }
    result = true;
  }

  return result;
}



char * serialRequest(const char * id, const char * attr, const char * value) { //returns pointer to response string on success or NULL on fail
  static char ser_responseString[serial_stringsize] = {'\0'};
  char ser_requestString[serial_stringsize] = {'\0'};

  strcpy(ser_requestString, id);
  strcat(ser_requestString, ".");
  strcat(ser_requestString, attr);
  if (value != NULL) {
    strcat(ser_requestString, "=");
    strcat(ser_requestString, value);
  }
  strcat(ser_requestString, "\n");

  //clear response buffer


  //clear receive buffer
  clearRx();

  //send request
  Serial.print(ser_requestString);

  //wait for response
  uint32_t timer = millis();
  bool result = false;
  while (millis() - timer < 500) { //timeout
    wdt_reset();
    result = readSerial(ser_responseString, serial_stringsize);
    if (result) break;
  }

  //return response
  if (result) {
    return ser_responseString;
  }
  else {
    return NULL;
  }
}

char * serialRequest(const char * id, const __FlashStringHelper* attr, const char * value) { //returns pointer to response string on success or NULL on fail
  char temp[50] = {'\0'};
  strcpy_P(temp, (PGM_P)attr);
  return serialRequest(id, temp, value);
}



#endif
