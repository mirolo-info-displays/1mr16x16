//@(#) i2cremote.h

#ifndef I2CREMOTE_H
#define I2CREMOTE_H

/* simple class for accessing 4 button I²C remote controls based on PCF8574 (buttons: red, green, left, right).
    Button hardware configuration:
    Bit 0: red button input (low active)
    Bit 1: red button LED (low active)
    Bit 2: green button input (low active)
    Bit 3: green button LED (low active)
    Bit 4: left button input (low active)
    Bit 5: left button LED (low active)
    Bit 6: right button input (low active)
    Bit 7: right button LED (low active)



    Usage:
    init i2cremote object with I²C address:
    remote i2cremote(ADDRESS);

    put update routine inside your mainloop:
    remote.Update();

    read button inputs and set lights:
    remote.setLightAll(true);

    remote.setLight(remote.red, true);
    remote.getButton(remote.red);
    remote.getButtonPressed(remote.red);
    remote.getButtonPressedLong(remote.red);
    remote.getButtonHoldLong(remote.red);
*/


class i2cremote
{
  public:
    enum button {red = 0, green = 2, left = 4, right = 6}; //button enumeration

  private:

    const uint16_t address;
    uint32_t timer_update = 0;
    uint32_t timer_hold[4] = {0};
    byte state_pressed = 0;
    byte state_pressed_long = 0;
    byte pcfdata = B11111111;


    int16_t setPCF(const byte & data) const {
      Wire.beginTransmission(address);            // begin the transmission to PCF8574
      Wire.write(data);
      if ( Wire.endTransmission() == 0) return 0; // end the Transmission
      else return -1;
    }


    int16_t getPCF(byte & data) const {
      Wire.requestFrom(address, 1); // request 1 byte from PCF
      if (Wire.available() == 1) {  // if the request is successfull
        data = Wire.read();         // receive the data
        return 0;
      }
      else {
        return -1;
      }
    }

    void softwareDebounce(const button & key) {
      if (getButton(key) && !(state_pressed & 0x01 << key) && !(state_pressed_long & 0x01 << key)) { //init
        state_pressed |= (0x01 << key);
        timer_hold[key / 2] = millis();
      }
      else if (getButton(key) && millis() - timer_hold[key / 2] >= 1000) { //long press
        state_pressed &= ~(0x01 << key);
        state_pressed_long |= (0x01 << key);
      }
      else if (!getButton(key) && ( (state_pressed & 0x01 << key) || (state_pressed_long & 0x01 << key) )) { //exit
        state_pressed &= ~(0x01 << key);
        state_pressed_long &= ~(0x01 << key);
      }
    }



  public:

    i2cremote(const uint16_t address) : address(address) {
    }



    void setLight(const button & key, const boolean & state) {

      if (!state) { //low active
        pcfdata |= (0x01 << key);
      }
      else {
        pcfdata &= ~(0x01 << key);
      }
    }

    void setLightAll(const boolean & state) {
      setLight(red, state);
      setLight(green, state);
      setLight(left, state);
      setLight(right, state);
    }

    boolean getLight(const button & key) {
      return !(boolean(pcfdata & 0x01 << key));
    }

    boolean getButton(const button & key) { //returns current button state
      return !(boolean(pcfdata & 0x02 << key));
    }

    boolean getButtonPressed(const button & key) { //returns true for one loop as soon as button is released
      return (!getButton(key) && (state_pressed & 0x01 << key));
    }

    boolean getButtonPressedLong(const button & key) { //returns true for one loop as soon as button is released after button hold >1000ms
      return (!getButton(key) && (state_pressed_long & 0x01 << key));
    }

    boolean getButtonHoldLong(const button & key) { //returns true if button is held down >1000ms
      return (getButton(key) && (state_pressed_long & 0x01 << key));
    }



    void Setup() {
      setPCF(pcfdata);
    }


    int16_t Update() { //button input debouncing and press duration detection. returns 0 on success, -1 on I²C error
      static int16_t result = 0;

      //software debounce
      softwareDebounce(red);
      softwareDebounce(green);
      softwareDebounce(left);
      softwareDebounce(right);


      static boolean toggle = 0;
      //update button state
      if (millis() - timer_update > 20) { //access I²C bus every 20ms

        if (toggle) {
          result = setPCF(pcfdata | B10101010);
          toggle = !toggle;
        }
        else{
          result = getPCF(pcfdata);
          toggle = !toggle;
        }

        timer_update = millis();
      }

      return result;
    }

};





#endif
