/*
  Firmware for mirolo 1MR16X16 led info panel

  Created 19. May. 2019
  by Simon Junga (https://gitlab.com/Theolyn)
*/

//set the unique serial number here to upload device specific calibration and mac address
//have a look at settings and definitions in global.h
#define SERIALNR 10
#define HTTPPASSWD "1346"
#define INITIAL_MSG_LINE0 "mirolo"

#define LOAD_EEPROM 1
#define LOAD_MODULE 0 //0 none, 1 schedule, 2 messageboard



//pins
const int pin_ws2812 = 14;
const int pin_lv = 0;
const int pin_onewire = 12;
const int pin_vsense = A0;




#include <ESP8266WiFi.h>
#include <EEPROM_Rotate.h>
#include <Wire.h>
#include <limits.h>
#include <ArduinoJson.h>
#include <urldecode.h>
#include <Base64.h>
#include <Time.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include "i2cremote.h"
#include "global.h"
#include "eeprom.h"
#include "serial.h"
//#include "http.h"
//#include "ntp.h"
//#include "sendHTML.h"


//byte mac[] = {0xDE, 0xB4, 0x78, 0x00, 0xE1, 0xF5};
//IPAddress ip = {192, 168, 130, 100};




//global variables

const uint16_t BLACK = matrix->Color(0, 0, 0);
const uint16_t GREEN = matrix->Color(0, 255, 0);
const uint16_t RED = matrix->Color(255, 0, 0);
const uint16_t PINK = matrix->Color(250, 101, 148);
const uint16_t BLUE = matrix->Color(0, 0, 255);
const uint16_t WHITE = matrix->Color(200, 200, 200);
const uint16_t YELLOW = matrix->Color(240, 200, 20);
const uint16_t PURPLE = matrix->Color(240, 40, 240);
const uint16_t ORANGE = matrix->Color(255, 160, 0);
const uint16_t TOMATO = matrix->Color(255, 50, 30);
const uint16_t TEAL = matrix->Color(0, 128, 80);
const uint16_t LIGHTGREEN = matrix->Color(160, 255, 110);
const uint16_t DARKGREEN = matrix->Color(12, 158, 17);
const uint16_t LIGHTBLUE = matrix->Color(100, 100, 180);



i2cremote remote(32);


// ******* setup
void setup() {

  //initialize inputs/outputs
  pinMode(pin_lv, OUTPUT);
  digitalWrite(pin_lv, HIGH);

  pinMode(pin_ws2812, OUTPUT);


  //settings from EEPROM
  EEPROMr.size(4);
  EEPROMr.begin(4096);

  if (LOAD_EEPROM) {
    loadEEPROM();
  }

  //init led matrix
  switch (state.rotation)
  {
  case up:
    matrix = new Adafruit_NeoMatrix(16, 16, pin_ws2812,
    NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
    NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
    NEO_GRB            + NEO_KHZ800);
    break;

  case left:
    matrix = new Adafruit_NeoMatrix(16, 16, pin_ws2812,
    NEO_MATRIX_TOP     + NEO_MATRIX_RIGHT +
    NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG,
    NEO_GRB            + NEO_KHZ800);
    break;

  case down:
    matrix = new Adafruit_NeoMatrix(16, 16, pin_ws2812,
    NEO_MATRIX_BOTTOM  + NEO_MATRIX_RIGHT +
    NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
    NEO_GRB            + NEO_KHZ800);
    break;

  case right:
    matrix = new Adafruit_NeoMatrix(16, 16, pin_ws2812,
    NEO_MATRIX_BOTTOM  + NEO_MATRIX_LEFT +
    NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG,
    NEO_GRB            + NEO_KHZ800);
    break;
  }

  matrix->begin();
  matrix->setTextWrap(false);
  matrix->setBrightness(state.brightness);
  matrix->setTextColor(WHITE);
  matrix->fillScreen(0);
  matrix->show();

  //add hardware description
  strcpy(mirolo.FW, __DATE__);
  strcat_P(mirolo.FW, PSTR(" | "));
  strcat(mirolo.FW, __TIME__);

  uint8_t counter = 0;
  uint8_t tmpMac[6] = {0};
  memcpy_P (tmpMac, mac, sizeof(tmpMac));
  for (uint8_t i = 0; i < 18; i += 3) {
    char tmp[2];
    sprintf(tmp, "%.2hhx", tmpMac[counter++]);
    mirolo.MAC[i] = tmp[0];
    mirolo.MAC[i + 1] = tmp[1];
    if (i < 15)mirolo.MAC[i + 2] = '-';
  }

  //Serial
  Serial.begin(serial_baud);

  //serial hello
  Serial.print(mirolo.HW);
  Serial.println(F(" led info panel"));
  Serial.println(F("Simon Junga | theolyn@is-a-furry.org"));
  Serial.println(F("_____________________________________"));
  Serial.println(F("Firmware Compile Date: "));
  Serial.println(mirolo.FW);
  Serial.println(F("Device Serial Number:"));
  Serial.println(mirolo.SNR);
  Serial.println(F("_____________________________________"));

  //serial hello
  Serial.println(F("Initializing I2C"));

  //init I²C
  Wire.begin(i2cAddress);    //join i2c bus
  //Wire.onRequest(wireRequest);   //register event for master request
  //Wire.onReceive(wireReceive);   //register event for receiving data

  //init I²C remote
  remote.Setup();

  /*
    //connect to wifi network
    Serial.println(F("Initializing Wireless..."));
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    //print IP info
    Serial.println(F(""));
    Serial.println(F("WiFi connected."));
    Serial.println(F("IP-Address: "));
    Serial.println(WiFi.localIP());
    Serial.println(F("OK\n"));
    server.begin();

    //start UDP
    Udp.begin(123);

    //init system time (NTP)
    setSyncProvider(getNtpTime);
    setSyncInterval(10); //10s

    uint32_t timestamp = millis();
    while (timeStatus() == timeNotSet && millis() - timestamp < 30000) {
      ESP.wdtFeed();
      //try obtaining time for 30s
    }

    //milClockUpdate();

    if (millis() - timestamp >= 30000) {
      setSyncInterval(300); //5min
      Serial.println("FAILED\n");
    }
    else {
      setSyncInterval(25200); //7h
      printTimeSerial();
      Serial.println("OK\n");
    }
  */

  //react on boot reason
  rst_info *resetInfo;
  resetInfo = ESP.getResetInfoPtr();

  if ( resetInfo->reason == REASON_EXT_SYS_RST || resetInfo->reason == REASON_DEFAULT_RST) {
    //normal startup. reset run

  }
  else {
    //system crashed.

  }


  //end serial hello
  Serial.println(F("Boot SUCCESSFULL"));
  Serial.println(F("Type \"help\" for a list of commands"));
  Serial.println(F("(Terminate serial commands with \\n)"));
  Serial.println(F("_____________________________________\n\n"));

  clearRx();


}









// ******* mainloop
void loop() {

  //load text presets
  switch (state.text)
  {
  case 0:
    text = &text_gc;
    break;

    case 1:
    text = &text_ef;
    break;
  
  default:
    text = &text_gc;
    break;
  }


  //menu select
  static uint32_t timer_select = millis();
  static boolean change_request = false;
  static uint8_t return_menu = 0;
  static int16_t text_position = matrix->width();

/*
  static uint8_t panel = 77;
  static uint32_t timer_menu = 0;
  if (millis() - timer_menu > 40) {
      //next one please
      matrix.fillScreen(BLACK);
        //matrix.fillTriangle(7, 14, 2, 9, 13, 9, BLUE);
        //matrix.fillTriangle(8, 14, 2, 9, 13, 9, BLUE);
        //matrix.fillTriangle(4, 7, 4, 15, 0, 11, BLUE);
        //matrix.fillTriangle(15, 7, 15, 15, 11, 11, BLUE);

      if (millis() - timer_menu > 60) {
        //text
        //const char message[] = {"Info      Lost+Found     Tickets"};
        char message[50] = {""};
        char tmp[10] = {'\0'};
        ltoa(panel, tmp, 10);
        strcat(message, tmp);

        matrix.setTextColor(WHITE);
        //matrix.fillScreen(BLACK);
        matrix.setCursor(-1, 5);
        matrix.print(message);
        //if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix.width())) text_position = matrix.width();

        timer_menu = millis();
        matrix.show();
      }
    }

    if (remote.getButtonPressed(remote.left)) {
      panel--;
    }
    else if (remote.getButtonPressed(remote.right)) {
      panel++;
    }



*/

  if (remote.getButtonHoldLong(remote.red) && !remote.getButton(remote.left)) {
    if (millis() - timer_select > 300) {
      state.brightness -= 10;
      if (state.brightness < 10) state.brightness = 10;
      timer_select = millis();
      matrix->setBrightness(state.brightness);
      settingsChanged = true;
    }
  }
  else if (remote.getButtonHoldLong(remote.green) && !remote.getButton(remote.left)) {
    if (millis() - timer_select > 300) {
      state.brightness += 10;
      if (state.brightness > 200) state.brightness = 200;
      timer_select = millis();
      matrix->setBrightness(state.brightness);
      settingsChanged = true;
    }
  }
  else if (remote.getButton(remote.left) && remote.getButtonPressed(remote.red)) {
    state.last_menu = 0;
    change_request = true;
    settingsChanged = true;
  }
  else if (remote.getButton(remote.left) && remote.getButtonPressed(remote.green)) {
    state.last_menu = 1;
    change_request = true;
    settingsChanged = true;
  }
  else if (remote.getButtonPressed(remote.red)) {
    state.menu = 0;
    settingsChanged = true;
  }
  else if (remote.getButtonPressed(remote.green)) {
    state.menu = 1;
    settingsChanged = true;
  }
  else if (remote.getButtonHoldLong(remote.left) && remote.getButtonHoldLong(remote.right)) {
    state.menu = 4;
  }
  else if (remote.getButtonPressed(remote.right) && (state.menu == 1 || (state.last_menu == 1 && state.menu >= 10))) {
    return_menu = state.menu;
    state.menu = 2;
    text_position = matrix->width();
    //settingsChanged = true;
  }
  else if (remote.getButtonPressedLong(remote.right) && state.menu >= 10) {

    state.sub_menu++;

    if (state.sub_menu > 17) {
      state.sub_menu = 10;
    }
    state.menu = state.sub_menu;

    return_menu = state.menu;
    state.menu = 3;
    settingsChanged = true;
  }
  else if (remote.getButtonPressed(remote.left) && !change_request) {
    if (state.menu <= 1) state.last_menu = state.menu; //save ticket counter state
    if (state.sub_menu < 10) state.sub_menu = 10; //init eeprom
    state.menu = state.sub_menu;
    settingsChanged = true;
  }
  else if (remote.getButtonHoldLong(remote.left) && !change_request) {
    state.last_menu = 255;
    settingsChanged = true;
  }
  else if (remote.getButtonPressedLong(remote.left) || remote.getButtonPressed(remote.left)) {
    change_request = false;
  }


  //menu
  static uint32_t timer_menu = millis();
  static uint32_t timer_blink = millis();

  if (state.menu == 0) {
    remote.setLightAll(false);
    remote.setLight(remote.red, true);

    if (millis() - timer_menu > 200) {
      //red X
      matrix->fillScreen(BLACK);
      matrix->drawLine(0, 1, 14, 15, RED);
      matrix->drawLine(0, 0, 15, 15, RED);
      matrix->drawLine(1, 0, 15, 14, RED);

      matrix->drawLine(0, 14, 14, 0, RED);
      matrix->drawLine(0, 15, 15, 0, RED);
      matrix->drawLine(1, 15, 15, 1, RED);
      matrix->show();

      timer_menu = millis();
    }

  }
  else if (state.menu == 1) {
    remote.setLightAll(false);
    remote.setLight(remote.green, true);

    static uint8_t counter = 0;
    if (millis() - timer_menu > 150) {
      //green arrow
      matrix->fillScreen(BLACK);
      matrix->setRotation(state.orientation);
      if (counter <= 0) {
        matrix->drawLine(0, -2, 7, 5, GREEN);
        matrix->drawLine(0, -3, 7, 4, GREEN);
        matrix->drawLine(0, -4, 7, 3, GREEN);

        matrix->drawLine(15, -2, 8, 5, GREEN);
        matrix->drawLine(15, -3, 8, 4, GREEN);
        matrix->drawLine(15, -4, 8, 3, GREEN);
      }

      else if (counter <= 1) {
        matrix->drawLine(0, 3, 7, 10, GREEN);
        matrix->drawLine(0, 2, 7, 9, GREEN);
        matrix->drawLine(0, 1, 7, 8, GREEN);

        matrix->drawLine(15, 3, 8, 10, GREEN);
        matrix->drawLine(15, 2, 8, 9, GREEN);
        matrix->drawLine(15, 1, 8, 8, GREEN);
      }

      else if (counter <= 35) {
        matrix->drawLine(0, 8, 7, 15, GREEN);
        matrix->drawLine(0, 7, 7, 14, GREEN);
        matrix->drawLine(0, 6, 7, 13, GREEN);

        matrix->drawLine(15, 8, 8, 15, GREEN);
        matrix->drawLine(15, 7, 8, 14, GREEN);
        matrix->drawLine(15, 6, 8, 13, GREEN);
      }
      matrix->setRotation(0);
      matrix->show();

      counter++;
      if (counter >= 40) counter = 0;
      timer_menu = millis();
    }

  }
  else if (state.menu == 2) {
    remote.setLightAll(false);


    if (millis() - timer_blink < 200) {
      remote.setLight(remote.right, true);
    }
    else if (millis() - timer_blink > 400) {
      timer_blink = millis();
    }

    static uint8_t counter = 0;
    if (millis() - timer_menu > 40) {
      //next one please
      matrix->fillScreen(BLACK);
      if (counter % 7 <= 2) {
        switch (state.orientation)
        {
        case up:
          matrix->fillTriangle(7, 14, 2, 9, 13, 9, BLUE);
          matrix->fillTriangle(8, 14, 2, 9, 13, 9, BLUE);
          break;
        case right:
          matrix->fillTriangle(4, 7, 4, 15, 0, 11, BLUE);
          matrix->fillTriangle(15, 7, 15, 15, 11, 11, BLUE);
          break;
        case down:
          matrix->fillTriangle(7, 9, 2, 14, 13, 14, BLUE);
          matrix->fillTriangle(8, 9, 2, 14, 13, 14, BLUE);
          break;
        case left:
          matrix->fillTriangle(0, 7, 0, 15, 4, 11, BLUE);
          matrix->fillTriangle(11, 7, 11, 15, 15, 11, BLUE);
          break;
        
        default:
          break;
        }
      }

      const char * message = text->t0;

      matrix->setTextColor(WHITE);
      matrix->setCursor(text_position, 0);
      matrix->print(message);
      text_position--;

      matrix->show();

      counter++;
      if (counter >= 80) {
        text_position = matrix->width();
        counter = 0;
        state.menu = return_menu;
      }
      timer_menu = millis();
    }

  }
  else if (state.menu == 3) {
    remote.setLightAll(false);


    if (millis() - timer_blink < 100) {
      remote.setLight(remote.right, true);
    }
    else if (millis() - timer_blink > 300) {
      timer_blink = millis();
    }

    static uint8_t counter = 0;
    if (millis() - timer_menu > 40) {
      //confirm blink
      counter++;
      if (counter >= 10) {
        counter = 0;
        state.menu = return_menu;
      }
      timer_menu = millis();
    }

  }
  else if (state.menu == 4) {
    remote.setLightAll(false);


    if (millis() - timer_blink < 800) {
      remote.setLightAll(true);
    }
    else if (millis() - timer_blink > 1600) {
      timer_blink = millis();
    }

    static uint8_t counter = 0;
    if (millis() - timer_menu > 200) {
      //display test
      matrix->fillScreen(BLACK);
      if (counter < 16) {
        matrix->drawLine(0, counter % 16, 15, counter % 16, RED);
        //matrix->drawLine(counter % 16, 0, counter % 16, 15, RED);
      }
      else if (counter < 32) {
        matrix->drawLine(0, counter % 16, 15, counter % 16, GREEN);
      }
      else if (counter < 48) {
        matrix->drawLine(0, counter % 16, 15, counter % 16, BLUE);
      }
      else if (counter < 64) {
        matrix->drawLine(0, counter % 16, 15, counter % 16, WHITE);
      }

      matrix->show();
      counter++;
      if (counter >= 64) {
        counter = 0;
      }
      timer_menu = millis();
    }

  }
  else if (state.menu >= 10) {
    remote.setLightAll(false);
    remote.setLight(remote.left, true);

    if (state.last_menu == 255 || millis() - timer_blink < 600) {
      if (state.last_menu == 0) remote.setLight(remote.red, true);
      else if (state.last_menu == 1) remote.setLight(remote.green, true);
    }
    else if (millis() - timer_blink > 1200) {
      timer_blink = millis();
    }

    boolean draw = false;
    if (state.menu == 10) {
      if (millis() - timer_menu > 60) {
        //text
        
        const char * message = text->t1;

        matrix->setTextColor(WHITE);
        matrix->fillScreen(BLACK);
        matrix->setCursor(text_position, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }

    if (state.menu == 11) {
      if (millis() - timer_menu > 60) {
        //text
        const char * message = text->t2;

        matrix->setTextColor(LIGHTBLUE);
        matrix->fillScreen(BLACK);
        matrix->setCursor(text_position, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }

    if (state.menu == 12) {
      if (millis() - timer_menu > 45) {
        //text
        const char * message = text->t3;

        matrix->setTextColor(ORANGE);
        matrix->fillScreen(BLACK);
        matrix->setCursor(text_position, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }

    if (state.menu == 13) {
      if (millis() - timer_menu > 45) {
        //text
        const char * message = text->t4;

        matrix->setTextColor(PURPLE);
        matrix->fillScreen(BLACK);
        matrix->setCursor(text_position, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }

    if (state.menu == 14) {
      if (millis() - timer_menu > 60) {
        //text
        const char * message = text->t5;

        matrix->setTextColor(PINK);
        matrix->fillScreen(BLACK);
        matrix->setCursor(text_position, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }
    
    if (state.menu == 15) {
      if (millis() - timer_menu > 60) {
        //text
        const char * message = text->t6;

        matrix->setTextColor(YELLOW);
        matrix->fillScreen(BLACK);
        matrix->setCursor(text_position, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }

    if (state.menu == 16) {
      if (millis() - timer_menu > 60) {
        //text
        const char * message = text->t7;

        matrix->setTextColor(TOMATO);
        matrix->fillScreen(BLACK);
        matrix->setCursor(5, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }

    if (state.menu == 17) {
      if (millis() - timer_menu > 60) {
        //text
        const char * message = text->t8;

        matrix->setTextColor(TEAL);
        matrix->fillScreen(BLACK);
        matrix->setCursor(text_position, 3);
        matrix->print(message);
        if (--text_position < (int16_t(0) - int16_t((strlen(message) * 7))) - int16_t(matrix->width())) text_position = matrix->width();

        draw = true;
        timer_menu = millis();
      }
    }

    if ((state.last_menu == 0 || state.last_menu == 1 ) && draw) {
      uint16_t color = BLACK;
      if (state.last_menu == 0){
        color = RED;
      }
      else if (state.last_menu == 1){
        color = GREEN;
      }

      switch (state.orientation)
        {
        case up:
          matrix->fillTriangle(0, 12, 3, 15, 6, 12, color);
          matrix->fillTriangle(9, 12, 12, 15, 15, 12, color);
          break;
        case right:
          matrix->fillTriangle(5, 15, 5, 11, 3, 13, color);
          matrix->fillTriangle(11, 15, 11, 11, 9, 13, color);
          break;
        case down:
          matrix->fillTriangle(0, 15, 3, 12, 6, 15, color);
          matrix->fillTriangle(9, 15, 12, 12, 15, 15, color);
          break;
        case left:
          matrix->fillTriangle(4, 15, 4, 11, 6, 13, color);
          matrix->fillTriangle(10, 15, 10, 11, 12, 13, color);
          break;
        
        default:
          break;
        }

      //matrix->drawLine(0, 14, 15, 14, RED);
      //matrix->drawLine(0, 15, 15, 15, RED);
      matrix->show();
    }
    else if (draw) {
      matrix->show();
    }

  }




  //DEBUG
  /*
    matrix->fillScreen(BLACK);
    matrix->drawLine(0, 1, 14, 15, RED);
    matrix->drawLine(0, 0, 15, 15, RED);
    matrix->drawLine(1, 0, 15, 14, RED);

    matrix->drawLine(0, 14, 14, 0, RED);
    matrix->drawLine(0, 15, 15, 0, RED);
    matrix->drawLine(1, 15, 15, 1, RED);
    matrix->show();
    portextender(32, 0, 1);
    delay(1000);

    matrix->fillScreen(BLACK);
    matrix->fillCircle(7, 8 , 4, ORANGE);
    matrix->fillCircle(7, 7 , 4, ORANGE);
    matrix->fillCircle(8, 8 , 4, ORANGE);
    matrix->fillCircle(8, 7 , 4, ORANGE);
    matrix->show();
    portextender(32, 0, 0);
    portextender(32, 4, 1);
    delay(1000);

    matrix->fillScreen(BLACK);
    matrix->drawLine(2, 9, 6, 13, GREEN);
    matrix->drawLine(3, 9, 7, 13, GREEN);

    matrix->drawLine(7, 13, 13, 2, GREEN);
    matrix->drawLine(8, 13, 12, 3, GREEN);
    matrix->show();
    portextender(32, 4, 0);
    portextender(32, 2, 1);
    delay(1000);

    matrix->fillScreen(BLACK);
    matrix->fillRect(6, 1, 4, 10, PURPLE);

    matrix->fillTriangle(7, 14, 2, 9, 13, 9, PURPLE);
    matrix->fillTriangle(8, 14, 2, 9, 13, 9, PURPLE);
    matrix->show();
    portextender(32, 2, 0);
    portextender(32, 6, 1);
    delay(1000);

    matrix->fillScreen(BLACK);
    matrix->drawChar(3, 1, 'A', LIGHTBLUE, BLACK, 2);
    matrix->show();
    portextender(32, 6, 0);
    delay(1000);



    int x    = matrix->width();
    static int pass = 0;
    matrix->setTextColor(WHITE);
    while (x > -80) {
      --x;
      matrix->fillScreen(0);
      matrix->setCursor(x, 5);
      matrix->print(F("hello world"));
      //if(--x < -80) {
      //x = matrix->width();
      //if(++pass >= 3) pass = 0;
      //matrix->setTextColor(PURPLE);
      //}
      matrix->show();
      delay(60);
    }


    delay(500);
    matrix->setTextColor(BLUE);
    matrix->fillScreen(0);
    matrix->setCursor(3, 5);
    matrix->print(F("23"));
    matrix->show();

    delay(1000);
  */


  /*
    // listen for incoming clients
    static boolean sendResponse = false;

    if (!sendResponse) {
      sendResponse = listenEthernet(server, client, method, url, url_size, pin, session, username, authOK, sessionOK, recv_content, recv_content_size);

      //DEBUG

          if (sendResponse) {
            Serial.print("method: ");
            Serial.println(method);
            Serial.print("url: ");
            Serial.println(url);
            Serial.print("content: ");
            Serial.println(recv_content);
            Serial.print("content_length: ");
            Serial.println(strlen(recv_content));
          }

    }
    //REST api calls
    else if (sendResponse && strstr_P(url, PSTR("/api/")) > NULL) {
      if (REST_api_enabled) {
        if (http_response(client, NULL, generateAPIreply)) {
          sendResponse = false;
          client.stop();
        }
      }
      else {
        if (http_response(client, 423, NULL)) {
          sendResponse = false;
          client.stop();
        }
      }
    }
    //index.html
    else if (sendResponse && method == GET && strstr_P(url, PSTR("/")) != NULL) {
      if (REST_html_enabled) {
        if (http_response(client, NULL, generateHTMLindex)) {
          sendResponse = false;
          client.stop();
        }
      }
      else {
        if (http_response(client, 423, NULL)) {
          sendResponse = false;
          client.stop();
        }
      }
    }
    //http 404 Not Found
    else if (sendResponse) {
      if (http_response(client, 404, NULL)) {
        sendResponse = false;
        client.stop();
      }
    }
  */





  //receive Serial input
  if (readSerial(ser_inputString, serial_stringsize)) {

    //reset
    char id[serial_stringsize] = {'\0'};
    char attribute[serial_stringsize] = {'\0'};
    char value[serial_stringsize] = {'\0'};
    uint16_t error = 0;

    char * token = strtok(ser_inputString, ".\n");
    if (token != NULL) {
      strcpy(id, token); //store string before "." to string
    }

    token = strtok(NULL, "=\n");
    if (token != NULL) {
      strcpy(attribute, token); //store string between "." and "=" to string
    }

    token = strtok(NULL, "\n");
    if (token != NULL) {
      strcpy(value, token); //store string after "=" to string
    }


    //process data
    if (strcmp_P(id, PSTR("help")) == 0) {
      Serial.print(F("List of commands:\n"));
      Serial.print(F("help                              Print this manual\n"));
      Serial.print(F("system.time                       Get unix time\n"));
      Serial.print(F("system.reboot                     Reboot system\n"));
      Serial.print(F("eeprom.save                       Save current settings to eeprom\n"));
      Serial.print(F("text.preset=[PRESET]              Load text preset. [gc, ef]\n"));
      Serial.print(F("matrix.rotation=[DIRECTION]       Rotation relative to the device's case. [up, right, down, left]\n"));
      Serial.print(F("matrix.orientation=[DIRECTION]    Orientation of animations relative to text. [up, right, down, left]\n"));
      Serial.print(F("\n\n"));

    }
    else if (strcmp_P(id, PSTR("system")) == 0 && strcmp_P(attribute, PSTR("time")) == 0 && strcmp_P(value, PSTR("")) == 0) {

      if (timeStatus() == timeNotSet) Serial.print("0\n");
      else {
        Serial.print(now());
        Serial.print("\n");
        Serial.print("ok.200\n");
      }
    }
    else if (strcmp_P(id, PSTR("system")) == 0 && strcmp_P(attribute, PSTR("reboot")) == 0 && strcmp_P(value, PSTR("")) == 0) {
      Serial.print("ok.200\n");
      Serial.flush();
      ESP.restart();
    }
    else if (strcmp_P(id, PSTR("eeprom")) == 0 && strcmp_P(attribute, PSTR("save")) == 0 && strcmp_P(value, PSTR("")) == 0) {
      updateEEPROM();
      Serial.print("ok.200\n");
    }
    else if (strcmp_P(id, PSTR("text")) == 0 && strcmp_P(attribute, PSTR("preset")) == 0 && strcmp_P(value, PSTR("")) != 0) {
      if(strcmp_P(value, PSTR("gc")) == 0){
        state.text = 0;
      }
      else if(strcmp_P(value, PSTR("ef")) == 0){
        state.text = 1;
      }
      else{
        error = 404;
      }

      if(error == 0){
        settingsChanged = true;
        Serial.print("ok.200\n");
      }   
    }
    else if (strcmp_P(id, PSTR("matrix")) == 0 && strcmp_P(attribute, PSTR("rotation")) == 0 && strcmp_P(value, PSTR("")) != 0) {
      if(strcmp_P(value, PSTR("up")) == 0){
        state.rotation = up;
      }
      else if(strcmp_P(value, PSTR("right")) == 0){
        state.rotation = right;
      }
      else if(strcmp_P(value, PSTR("down")) == 0){
        state.rotation = down;
      }
      else if(strcmp_P(value, PSTR("left")) == 0){
        state.rotation = left;
      }
      else{
        error = 404;
      }

      if(error == 0){
        Serial.print("ok.200\n");
        updateEEPROM();
        Serial.flush();
        ESP.restart();
      }   
    }
    else if (strcmp_P(id, PSTR("matrix")) == 0 && strcmp_P(attribute, PSTR("orientation")) == 0 && strcmp_P(value, PSTR("")) != 0) {
      if(strcmp_P(value, PSTR("up")) == 0){
        state.orientation = up;
      }
      else if(strcmp_P(value, PSTR("right")) == 0){
        state.orientation = right;
      }
      else if(strcmp_P(value, PSTR("down")) == 0){
        state.orientation = down;
      }
      else if(strcmp_P(value, PSTR("left")) == 0){
        state.orientation = left;
      }
      else{
        error = 404;
      }

      if(error == 0){
        settingsChanged = true;
        Serial.print("ok.200\n");
      }   
    }
    else {
      error = 400;
    }

    //error status code (http)
    if (error > 0) {
      Serial.print(F("error."));
      char tmp[10] = {'\0'};
      ltoa(error, tmp, 10);
      Serial.print(tmp);
      Serial.print("\n");
    }
  }




  //update remote input
  remote.Update();


  //update EEPROM if necessary
  static uint32_t timer_eeprom = 0;
  static uint8_t eeprom_counter = 0;
  if (settingsChanged == true) {
    settingsChanged = false;
    if (eeprom_counter < 100) {
      eeprom_counter++; //safety just in case there is something constantly updating faster than the 10s delay
      timer_eeprom = millis() - (120000UL - 10000); //update settings immediately 10s after last change request
    }
  }
  if (millis() - timer_eeprom >= 120000UL) {
    updateEEPROM();
    eeprom_counter = 0;
    timer_eeprom = millis();
  }


}










// ******* functions

//print current time and date to serial
void printTimeSerial() {
  Serial.print("Unix Time: ");
  Serial.println(now());
  Serial.print(day());
  Serial.print(".");
  Serial.print(month());
  Serial.print(".");
  Serial.print(year());
  Serial.print("  ");
  if (hour() < 10) Serial.print("0");
  Serial.print(hour());
  Serial.print(":");
  if (minute() < 10) Serial.print("0");
  Serial.println(minute());
}
