//@(#) http.h

#ifndef HTTP_H
#define HTTP_H

const uint8_t read_block_size = 30; //number of chars that are read from a client in a row before mainloop continues (performance critical, decrease if mainloop hangs on client.read)
enum httpMethod : uint8_t {UNDEFINED = 0, GET, DELETE, POST, PATCH, UPDATE, PUT, OPTIONS};



//create random session key (16 characters null terminated string)
void createSession(char * session) {
  for (uint8_t i = 0; i < 16; i++) {
    session[i] = char(random(33, 127));
  }
  session[16] = '\0';
}


//http redirect
boolean http_redirect(WiFiClient &client, const char * url, const char * session_key) { //send http redirect header to client and set session key (if any)
  boolean result = false;

  //send http header
  client.print(F("HTTP/1.1 303"));
  client.print(F(" See Other\r\n"));
  client.print(F("Location: "));
  client.print(url);
  client.print(F("\r\n"));
  if (session_key) {
    client.print(F("Set-Cookie: mirolosession="));
    client.print(session_key);
    client.print(F("; Max-Age=86400; HttpOnly\r\n"));
  }
  client.print(F("Connnection: close\r\n"));
  client.print(F("\r\n"));
  result = true;

  return result;
}




//http authentification request
boolean http_authrequest(WiFiClient &client, const char * session_key) { //send http authentification request to client and set session key (if any)
  boolean result = false;

  //send http header
  client.print(F("HTTP/1.1 401 Authorization Required\r\n"));
  client.print(F("WWW-Authenticate: Basic realm=\"Username: The name you like others to see when accessing this display while you are still logged in. Password: 4 digit PIN set for this display.\"\r\n"));
  if (session_key) {
    client.print(F("Set-Cookie: mirolosession="));
    client.print(session_key);
    client.print(F("; Max-Age=86400; HttpOnly\r\n"));
  }
  client.print(F("Connnection: close\r\n"));
  client.print(F("\r\n"));
  result = true;

  return result;
}


//http response with content
boolean http_response(WiFiClient &client, const uint16_t &statusCode, unsigned int (*generateContentFunction)(WiFiClient &, const boolean &, char *, uint16_t *)) { //send http header + response (function pointer of HTML generator) to client
  static unsigned int response_length = 0;
  static unsigned int response_length_sent = 0;
  static char content_type[31] = {'\0'};
  static uint16_t http_status = 500;
  boolean result = false;

  //send http header
  if (response_length == 0) {

    //write status code. contentGenerator will always overwrite this
    if (&statusCode != NULL) http_status = statusCode;

    //check for NULL pointer and just send http 200 OK
    if (generateContentFunction != NULL) response_length = generateContentFunction(client, false, content_type, &http_status);
    else result = true;

    //check if message length has been set or just return http header with content-length: 0
    if (response_length == 0) result = true;

    client.print(F("HTTP/1.1 "));
    client.print(http_status);
    if (http_status == 200)client.print(F(" OK\r\n"));
    else if (http_status == 400)client.print(F(" Bad Request\r\n"));
    else if (http_status == 403)client.print(F(" Forbidden\r\n"));
    else if (http_status == 404)client.print(F(" Not Found\r\n"));
    else if (http_status == 413)client.print(F(" Payload Too Large\r\n"));
    else if (http_status == 423)client.print(F(" Locked\r\n"));
    else if (http_status == 500)client.print(F(" Internal Server Error\r\n"));
    client.println(F("Access-Control-Allow-Origin: *"));
    client.println(F("Access-Control-Allow-Methods: GET,PUT,POST,DELETE, PATCH"));
    client.println(F("Access-Control-Allow-Headers: Content-Type"));
    if (generateContentFunction != NULL && result == false) {
      client.print(F("Content-Type: "));
      client.print(content_type);
      client.print(F("\r\n"));
    }
    client.print(F("Connection: close\r\n"));
    if (generateContentFunction != NULL && result == false) {
      client.print(F("Content-Length: "));
      client.print(response_length);
      client.print(F("\r\n"));
    }
    client.print("\r\n");

  }
  //send content
  else {
    response_length_sent += generateContentFunction(client, true, content_type, &http_status);
    //everything sent. reset variables
    if (response_length_sent >= response_length) {
      result = true;
      response_length = 0;
      response_length_sent = 0;
    }
  }

  if (result == true) {
    strcpy_P(content_type, PSTR(""));
    http_status = 500;
  }
  return result;
}






//wait for incoming client requests. returns true if client has been read successfully
boolean listenEthernet(WiFiServer &server, WiFiClient &client, uint8_t &method, char * url, const uint16_t &url_size, char * pin_key, char * session_key, char * username, boolean &authentificated,  boolean &session_active, char * content, const uint16_t &content_size) {
  static uint8_t methodOK = 0; //0:reading, 1:ok, 2:failed
  static uint8_t urlOK = 0; //0:reading, 1:ok, 2:failed
  static boolean lineBLANK = false;
  static uint16_t counter = 0;
  static uint8_t state = 0;
  static char linebuf[100];
  static char * user = NULL;
  static char * passwd = NULL;
  static uint8_t charcount = 0;
  boolean result = false;

  //listen for incoming clients
  if (state == 0 && !client) {
    client = server.available();
    if (client) {
      state = 10;
      //reset
      authentificated = false;
      session_active = false;
      if (user) {
        delete [] user;
        user = NULL;
      }
      if (passwd) {
        delete [] passwd;
        passwd = NULL;
      }
      methodOK = 0;
      urlOK = 0;
      lineBLANK = false;
      counter = 0;
    }

  }
  //read from client
  else if ((state == 10 || state == 11 || state == 2) && client) {

    //read http header
    if ((state == 10 || state == 11) && client.connected()) {
      uint8_t readcount = 0;
      while (client.available() && readcount < read_block_size && (state == 10 || state == 11)) {
        char c = client.read();
        linebuf[charcount] = c;
        linebuf[charcount + 1] = '\0';

        if (charcount < sizeof(linebuf) - 1) {
          charcount++;
          if (state == 10 && strstr_P(linebuf, PSTR("HTTP/")) > NULL) {
            state = 11;
          }
        }

        //buffer overflow on first line or line without 'HTTP/' -> not a http message. read message and return as ICMP echo (ping)
        else if (state == 10 && ((charcount >= sizeof(linebuf) - 1) || c == '\n')) {
          //not a http message. return ping
          strcpy(content, linebuf);
          counter = strlen(content);
          state = 3;
        }

        //double newline ends http request
        if (c == '\n' && lineBLANK) {
          //copy username
          if (authentificated && session_active) strcpy(username, user);

          //GET, DELETE or OPTIONS. no need to read further
          if (methodOK == 1 && (method == GET || method == DELETE || method == OPTIONS) && urlOK == 1) {
            while (client.available()) client.read(); //empty buffer
            result = true;
            state = 0;
          }
          //POST or else. read client message
          else if (methodOK == 1 && urlOK == 1) {
            state = 2;
          }
          //something went wrong. start over
          else {
            while (client.available()) client.read(); //empty buffer
            state = 0;
            http_response(client, 400, NULL); //notify client
            client.stop();
          }
        }
        if (c == '\n') {
          //newline found
          lineBLANK = true;

          //http method
          if (methodOK == 0) {
            char line[100];  //copy of linebuffer
            char * subString = NULL; // pointer to the beginning of the substring

            strcpy(line, linebuf);
            subString = strtok(line, " "); // find the first space
            if (subString != NULL) {
              methodOK = 1;
              if (strcmp_P(line, PSTR("GET")) == 0) method = GET;
              else if (strcmp_P(line, PSTR("DELETE")) == 0) method = DELETE;
              else if (strcmp_P(line, PSTR("POST")) == 0) method = POST;
              else if (strcmp_P(line, PSTR("PATCH")) == 0) method = PATCH;
              else if (strcmp_P(line, PSTR("UPDATE")) == 0) method = UPDATE;
              else if (strcmp_P(line, PSTR("PUT")) == 0) method = PUT;
              else if (strcmp_P(line, PSTR("OPTIONS")) == 0) method = OPTIONS;
              else method = UNDEFINED;
            }
            else {
              methodOK = 2;
            }



            //http url
            if (urlOK == 0) {
              subString = strtok(NULL, " "); // find the second space
              if (subString != NULL && strlen(subString) <= url_size) {
                urlOK = 1;
                strcpy(url, subString);
              }
              else {
                urlOK = 2;
              }
            }
          }


          //authentification
          if (authentificated == false && strstr_P(linebuf, PSTR("Authorization: Basic ")) > NULL) {
            //decode USER:PASSWORD from base64 and check for validity
            char line[100];  //copy of linebuffer
            strcpy(line, linebuf);

            //find encoded String after "Authorization: Basic "
            char * encoded = strtok(line, " ");
            encoded = strtok(NULL, " ");
            encoded = strtok(NULL, " \r");

            char * decoded;
            uint8_t decoded_length = Base64.decodedLength(encoded, strlen(encoded));
            decoded = new char[decoded_length + 1];
            Base64.decode(decoded, encoded, strlen(encoded));

            user = new char[decoded_length + 1];
            passwd = new char[decoded_length + 1];

            strcpy(user, strtok(decoded, ":"));
            strcpy(passwd, strtok(NULL, ""));



            if (strcmp(pin_key, passwd) == 0 && strlen(user) <= 20 && strcmp_P(user, PSTR("")) != 0) authentificated = true;
            delete [] decoded;
          }
          if (session_active == false && strstr_P(linebuf, PSTR("Cookie: mirolosession=")) > NULL && strlen(session_key) > 1 && strstr(linebuf, session_key) > NULL) session_active = true;



          //reset linebuffer
          memset(linebuf, '\0', sizeof(linebuf));
          charcount = 0;

        } else if (c != '\r') {
          //character found on current line
          lineBLANK = false;
        }

        ++readcount;
      }
    }

    //read content
    else if (state == 2 && client.connected()) {
      uint8_t readcount = 0;
      if (client.available()) {
        while (client.available() && readcount < read_block_size && state == 2) {
          char c = client.read();

          if (counter < content_size - 1) {
            content[counter++] = c;
          }
          //message didn't fit the content buffer
          else {
            while (client.available()) client.read(); //empty buffer
            content[counter - 1] = '\0';
            state = 0;
            http_response(client, 413, NULL); //notify client
            client.stop();
          }

          ++readcount;
        }
      }
      else {
        content[counter] = '\0';
        result = true;
        state = 0;
      }

    }

    //return ICMP echo (ping)
    else if (state == 3 && client.connected()) {
      uint8_t readcount = 0;
      if (client.available()) {
        while (client.available() && readcount < read_block_size && state == 3) {
          char c = client.read();

          if (counter < content_size - 1) {
            content[counter++] = c;
          }
          //message didn't fit the content buffer
          else {
            while (client.available()) client.read(); //empty buffer
            content[counter - 1] = '\0';
            state = 0;
            http_response(client, 413, NULL); //notify client
            client.stop();
          }

          ++readcount;
        }
      }
      else {
        //return echo
        client.write(content, counter - 1);
        client.stop();
        content[counter] = '\0';
        state = 0;
      }

    }

    //client disconnected. start over
    else if (!client.connected()) {
      state = 0;
      if (client) client.stop();
    }

  }
  else {
    state = 0;
    if (client) client.stop();
  }

  return result;

}

#endif
