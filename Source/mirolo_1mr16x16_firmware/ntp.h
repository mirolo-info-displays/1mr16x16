//@(#) ntp.h

#ifndef NTP_H
#define NTP_H

#include <WiFiUdp.h>

WiFiUDP Udp;

const byte ntp_packet_size = 48; //NTP time is in the first 48 bytes of message
uint16_t ntp_sync_attempts = 0; //NTP synchronization attempts. A value higher than 0 indicates the number of failed attempts in a row


boolean summertime_RAMsave(int year, byte month, byte day, byte hour, byte tzHours)
// European Daylight Savings Time calculation by "jurs" for German Arduino Forum
// input parameters: "normal time" for year, month, day, hour and tzHours (0=UTC, 1=MEZ)
// return value: returns true during Daylight Saving Time, false otherwise
{
  if (month < 3 || month > 10) return false; // keine Sommerzeit in Jan, Feb, Nov, Dez
  if (month > 3 && month < 10) return true; // Sommerzeit in Apr, Mai, Jun, Jul, Aug, Sep
  if ( (month == 3 && (hour + 24 * day) >= (1 + tzHours + 24 * (31 - (5 * year / 4 + 4) % 7)) ) || (month == 10 && (hour + 24 * day) < (1 + tzHours + 24 * (31 - (5 * year / 4 + 1) % 7)) ) )
    return true;
  else
    return false;
}



void sendNTPpacket(IPAddress &address, byte * packetBuffer)// send an NTP request to the time server at the given address
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, ntp_packet_size);

  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  //send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, ntp_packet_size);
  Udp.endPacket();
}



time_t getNtpTime() {

  unsigned long secsSince1900 = 0UL;
  byte packetBuffer[ntp_packet_size] = {0}; //buffer to hold incoming & outgoing packets

  //discard any previously received packets
  while (Udp.parsePacket() > 0);

  sendNTPpacket(ntpIP, packetBuffer);

  uint32_t beginWait = millis();
  while (millis() - beginWait < 400) {
    int size = Udp.parsePacket();
    if (size >= ntp_packet_size) {

      Udp.read(packetBuffer, ntp_packet_size);  // read packet into the buffer

      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      unsigned long epochUnix = secsSince1900 - 2208988800UL; //subtract 70 years

      ///timezone adjustment
      if (summertime_RAMsave(year(epochUnix), month(epochUnix), day(epochUnix), hour(epochUnix), 1)) {
        epochUnix += timeZone * SECS_PER_HOUR;
      }
      else {
        epochUnix += (timeZone - 1) * SECS_PER_HOUR;
      }

      //reject date codes lower than 12.4.2018  1:30
      if (epochUnix > 1523496649) {
        ntp_sync_attempts = 0;
        setSyncInterval(25200); //7h
        return epochUnix;
      }
      else {
        setSyncInterval(300); //5min
        ++ntp_sync_attempts;
        return 0;
      }
    }
  }
  setSyncInterval(300); //5min
  ++ntp_sync_attempts;
  return 0; // return 0 if unable to get the time
}




#endif
